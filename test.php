<?php

function getCookieByHttps($url = '') {
	// 指定cookie文件存放地址
	// var_dump(sys_get_temp_dir());
	$cookie_file = tempnam(sys_get_temp_dir(), "cookies");
	$ch = curl_init($url); //初始化
	$header = [
		'Host: xueqiu.com',
		'Connection: keep-alive',
		'Cache-Control: max-age=0',
		'Upgrade-Insecure-Requests: 1',
		'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
		'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		'Accept-Encoding: gzip, deflate, sdch, br',
		'Accept-Language: zh-CN,zh;q=0.8,en;q=0.6',
		// 'Cookie: s=5p111m93tt; xq_a_token=e8e54f45363e45c762c42cd926e470175c1123a1; xq_r_token=36c71dba39380a7c439676ea63f63f717faa677b; Hm_lvt_1db88642e346389874251b5a1eded6e3=1480674296; Hm_lpvt_1db88642e346389874251b5a1eded6e3=1480674441; __utma=1.1684846677.1480674296.1480674296.1480674296.1; __utmc=1; __utmz=1.1480674296.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)',
	];
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在
	curl_setopt($ch, CURLOPT_HEADER, 0); //不返回header部分
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //返回字符串，而非直接输出
	curl_setopt($ch, CURLOPT_ENCODING, ''); //设置解码方式
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file); //存储cookies到指定的临时文件中

	curl_exec($ch);
	if ($error = curl_error($ch)) {
		die($error);
	}
	curl_close($ch);
	return $cookie_file;
}

function curl_https($url, $cookie_file = '') {

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在

	curl_setopt($ch, CURLOPT_URL, $url);
	// 设定curl不返回头信息
	$header = [
		'Host: xueqiu.com',
		'Connection: keep-alive',
		'Cache-Control: max-age=0',
		'Upgrade-Insecure-Requests: 1',
		'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
		'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		'Accept-Encoding: gzip, deflate, sdch, br',
		'Accept-Language: zh-CN,zh;q=0.8,en;q=0.6',
		// 'Cookie: s=5p111m93tt; xq_a_token=e8e54f45363e45c762c42cd926e470175c1123a1; xq_r_token=36c71dba39380a7c439676ea63f63f717faa677b; Hm_lvt_1db88642e346389874251b5a1eded6e3=1480674296; Hm_lpvt_1db88642e346389874251b5a1eded6e3=1480674441; __utma=1.1684846677.1480674296.1480674296.1480674296.1; __utmc=1; __utmz=1.1480674296.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)',
	];
	curl_setopt($ch, CURLOPT_HEADER, 0);
	// HTTP请求头中"Accept-Encoding: "的值。 这使得能够解码响应的内容。
	// 支持的编码有"identity"，"deflate"和"gzip"。如果为空字符串""，会发送所有支持的编码类型。
	curl_setopt($ch, CURLOPT_ENCODING, '');
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	// 携带cookie访问指定页面
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);

	$response = curl_exec($ch);

	if ($error = curl_error($ch)) {
		die($error);
	}
	curl_close($ch);
	return $response;
}

$cookie_file = getCookieByHttps('https://xueqiu.com/hq#exchange=CN&firstName=1&secondName=1_0');
var_dump($cookie_file);

$data = curl_https('https://xueqiu.com/stock/cata/stocklist.json?page=1&size=90&order=desc&orderby=percent&type=11%2C12&_=1480679815319', $cookie_file);

echo $data;
