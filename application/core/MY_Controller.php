<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	/**
	 * 构造方法
	 */
	public function __construct() {
		// 继承父类构造方法
		parent::__construct();
		// 装载相关数据模型
		$this->load->model('Stocks');
		$this->load->model('Symbol');
		$this->load->model('Curlmodel');
		// $this->cookie_file = $this->Curlmodel->getCookieByHttps('https://xueqiu.com/S/SZ300059');
	}

	/**
	 * [loadview 将头部、底部脚本统一装载]
	 * @param  [type] $view [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function loadview($view = '', $data = array()) {
		$this->load->view('head');
		$this->load->view($view, $data);
		$this->load->view('foot');
	}

}
