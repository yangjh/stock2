!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>正在抓取……<small>从雪球行情中心中获取指定页面的信息</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-info">
            <div class="box-body">
                <div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" style="width: 0"></div></div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
// 执行抓取任务
    $(function() {
        // 设置当前激活菜单样式
        // console.log('test');
        $('#curl').addClass('active');
        // setInterval(getProcesing, 1000);
        // 页面载入后立即开始执行抓取任务
        $.post('<?php echo site_url('curl/getMultiStockName/' . $start . '/' . $end) ?>',
            function(data, textStatus, xhr) {
                layer.msg('抓取成功！');
                // location.href = '<?php echo site_url("curl/displayMultiStocks") ?>';
            });
        // getProcesing();
    });

    // 执行获取进度功能
    $(function(){
        var start = <?php echo $start; ?>;
        // console.log(start);
        var end = <?php echo $end; ?>;
        // console.log(end);
        // 定义处理获取进度的函数
        function getProcesing() {
            // 获取ci中循环的进度，我通过两天的摸索，发现如果使用ci框架，当同一站点的某个ci任务没有完成时，
            // 不管使用文件、全局变量、数据库、cookie、session等何种方式，都不能在循环正在进行时，获取循环的进度信息
            // ci大致要在一个任务完成后，才进行下一个任务。
            // 因此，我决定绕开ci的流程，另起炉灶，不用ci框架，则可获取循环中的进度信息，从而实现mvc模式分离下的进度信息显示。
            // 比如下面的方式就是使用纯粹的文件形式读取进度信息，以后还可将文件的格式存储为json或者数组，以便存储更为复杂的信息。
            var currentPercent = '<?php echo base_url('application/tmp/stockpage' . $start . $end . '.txt') ?>';
            // console.log(currentPercent);
            $.getJSON(currentPercent,function(data) {
                // console.log(data);
                $('.progress-bar').attr( "style", "width:" +  data + "%");
                $('.progress-bar').text( data + "%");
            }).done(function(data) {
                // console.log('it works!');
                // console.log(data);
                // 如果任务完成，则停止执行任务
                if(data === 100) {
                    clearInterval(int);
                };
            });
        };
        // 定时执行获取函数
        var int = setInterval(getProcesing, 1000);
    });
</script>


