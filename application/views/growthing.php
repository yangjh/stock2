!-- Content Wrapper. Contains page content -->
<script src="<?php echo base_url('application/views/table/bootstrap-table.js'); ?>"></script>
<link href="<?php echo base_url('application/views/table/bootstrap-table.css'); ?>" rel="stylesheet">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>连续增长公司一览表<small>显示所有连续增长的关键成长性信息</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-info">
            <div class="panel panel-default">
                <div><button type="button" class="btn btn-primary" id="add2selected">添加到自选股</button></div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="table" data-toggle="table" data-height="460" data-url="<?php echo site_url('analytics/jsonData/') ?>" data-sort-name="sustainedGrowth" data-sort-order="desc" data-click-to-select="true">
                            <thead>
                                <tr>
                                    <th data-field="state" data-checkbox="true"></th>
                                    <th data-field="symbol" data-sortable="true">股票代码</th>
                                    <th data-field="sname" data-sortable="true">股票名称</th>
                                    <th data-field="sustainedGrowth" data-sortable="true">连增季度数</th>
                                    <th data-field="xsmlv" data-sortable="true">销售毛利率</th>
                                    <th data-field="jlrhjl" data-sortable="true">净利润含金量</th>
                                    <th data-field="marketCapital" data-sortable="true">市值</th>
                                    <th data-field="marketCapital" data-sortable="true">三年净资产收益率</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- end of .panel-body -->
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
var $table = $('#table'),
    $data,
    $button = $('#add2selected');

$(function() {
    $button.click(function() {
        $data = $table.bootstrapTable('getSelections');

        var i = $data.length;
        var $symbol = '';
        while (i--) {
            $symbol = $symbol + $data[i].symbol + ',';
        }
        // 取掉最后的逗号
        $symbol = $symbol.substring(0, $symbol.length - 1);

        $.post("<?php echo site_url('selected/updateMultiSelected') ?>", {
            symbols: $symbol
        }, function() {
            layer.msg('股票' + $symbol + '已添加到自选股');
        });
        // console.log($data);
    });
});
</script>
