  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>安装指南<small>成长股投资体系程序安装及配置指南</small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <h2>简介</h2>
      <p>成长股投资体系程序基于企业基本面，可从全部上市公司中筛选出连续多个季度保持高速增长的公司，再根据历史估值，判断其是否具有投资价值，力图构建出一套选股、买卖点、仓位控制的投资体系。</p>
      <h2>下载</h2>
      <p>该程序为开源软件，项目地址为：<a href="http://git.oschina.net/yangjh/stock2">http://git.oschina.net/yangjh/stock2</a></p>
      <p><a href="http://git.oschina.net/yangjh/stock2/repository/archive/master" class="btn btn-primary"><i class="fa fa-download"></i> Download</a></p>
      <h2>安装</h2>
      <ol>
        <li>解压到服务器web目录，例如：<code>c:\wamp\www</code>；</li>
        <li>打开位于<code>application\config</code>目录中的<code>database.php</code>，根据实际情况，修改数据库配置信息，填写诸如用户名、密码、数据库名称等信息。
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> 注意</h4>
                需使用数据库管理工具phpMyAdmin之类的工具，提前建立数据库，然后将数据库填写在配置文件中。
            </div>
        <pre><code>$db['default'] = array(
          'dsn' => '',
          'hostname' => 'localhost',
          'username' => 'root',
          'password' => '',
          'database' => 'stock2016',
          'dbdriver' => 'mysqli',</code></pre></li>
          <li>运行<a href="<?php echo site_url('curl/displayClearTool') ?>">数据抓取模块</a>，开始抓取数据。</li>
      </ol>
      <h2>工作流程</h2>
      <ol>
        <li>抓取公司名称</li>
        <li>抓取公司财务信息</li>
        <li>分析财务信息</li>
        <li>查看分析结果</li>
        <li>添加到自选股</li>
        <li>查看自选股信息</li>
      </ol>

      <!-- Your Page Content Here -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
  // 设置当前激活菜单样式
    $(function() {
        $('#setup').addClass('active');
        }
    );
  </script>
