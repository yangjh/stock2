  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>清空数据表<small>清空已有数据表或者新建数据表</small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-warning">
        <div class="box-body">
              <div class="alert alert-info row">
                <h4><i class="icon fa fa-info"></i> 注意：此项功能将清空所有历史数据，请慎重使用！</h4>
                <div class="col-md-6 col-md-offset-3">
                  <button type="button" class="btn btn-block btn-danger btn-lg" id="go">我知道了，开始清空</button>
                </div>
            </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
    $(function() {
        // 设置当前激活菜单样式
        $('#curl').addClass('active');
        // 为按钮添加事件
        $("#go").on("click", function () {
           $.post('<?php echo site_url("curl/clearStock") ?>', function(data, textStatus, xhr) {
                layer.msg('股票数据库重置成功！');
                location.href='<?php echo site_url("curl/displayMultiStocks") ?>';
            });
        })
      }
    );
  </script>
