!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>为信息总表添加每股净资产信息<small>逐条检查公司最新报表中的每股净资产信息，将其更新到公司数据总表中。</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-info">
            <div class="box-body">
                <div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped active" style="width: 0"></div></div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
// 执行抓取任务
    $(function() {
        // 设置当前激活菜单样式
        // console.log('test');
        $('#curl').addClass('active');
        // setInterval(getProcesing, 1000);
        // 页面载入后立即开始执行分析检查任务
        $.post('<?php echo site_url('curl/repaireStocksMgjzc') ?>',
            function(data, textStatus, xhr) {
                layer.msg('更新完毕。');
                // location.href = '<?php echo site_url("curl/displayMultiStocks") ?>';
            });
        // getProcesing();
    });
</script>


