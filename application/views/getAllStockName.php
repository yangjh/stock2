<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>抓取上市公司名称及代码<small>将从雪球的行情中心中获取全部上市公司的名称和代码</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-info">
            <div class="box-body">
            <ol>

                <?php for ($i = 1; $i < count($step); $i++) {
	$url = 'curl/displayStockProgress/' . $step[$i - 1] . '/' . $step[$i];
	?>
                <li><a href="<?php echo site_url($url) ?>" target="_black">抓取<?php echo $step[$i - 1] . '-' . $step[$i] ?>页公司信息</a></li>
                <?php }?>
                </ol>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
$(function() {
    // 设置当前激活菜单样式
    $('#curl').addClass('active');
    // 为按钮添加事件
    $("#go").on("click", function() {
        $.post('<?php echo site_url("curl/clearStock") ?>', function(data, textStatus, xhr) {
            layer.msg('股票数据库重置成功！');
            location.href = '<?php echo site_url("curl/displayMultiStocks") ?>';
        });
    })
});
</script>
