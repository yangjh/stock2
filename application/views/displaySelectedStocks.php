  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>自选股<small>考虑成长性和估值因素选择的股票</small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="panel panel-default">
          <div class="panel-heading">自选股</div>
          <div class="panel-body">
              <div class="table-responsive">
                  <table class="table">
                      <thead>
                          <tr>
                              <th>序号</th>
                              <th>Code</th>
                              <th>Name</th>
                              <th>NA</th>
                              <th>Price</th>
                              <th>PB</th>
                              <th>PBRange</th>
                              <th>PE</th>
                              <th>市值</th>
                              <th>PBHis</th>
                              <th>雪球</th>
                              <th>连增数</th>
                              <th>毛利率</th>
                              <th>含金量</th>
                              <th>网格间距</th>
                              <th>操作建议</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php foreach ($selectedStocks as $k => $v): ?>
                          <tr id="<?php echo $v['symbol'] ?>">
                              <td class="id">
                                  <a href="javascript:;">
                                      <?php echo $k ?>
                                  </a>
                              </td>
                              <td class="symbol">
                                  <a href="">
                                      <?php echo $v['scode'] ?>
                                  </a>
                              </td>
                              <td class="scode">
                                  <a href="<?php echo site_url('analytics/displayStockInfoByCode/' . $v['scode']); ?>">
                                      <?php echo $v['sname'] ?>
                                  </a>
                              </td>
                              <td class="netAssets">
                                  <a href="javascript:;">
                                      <?php echo $v['mgjzc'] ?>
                                  </a>
                              </td>
                              <td class="price"></td>
                              <td class="pb"></td>
                              <td class="pbRange">
                                  <?php echo round($v['lowpb'], 1) . '-' . round($v['toppb'], 1) ?>
                              </td>
                              <td class="pe"></td>
                              <td class="marketCapital"></td>
                              <td class="historyPB">
                                  <a href="javascript:;" data-scode="<?php echo $v['scode'] ?>"><img src="http://www.sina.com.cn/favicon.ico" width="16" /></a>
                              </td>
                              <td>
                                  <a href="<?php echo 'http://www.xueqiu.com/S/' . $v['symbol'] ?>" target="_black"><img src="http://www.xueqiu.com/favicon.ico" width="16" /></a>
                              </td>
                              <td>
                                  <?php echo $v['sustainedGrowth'] ?>
                              </td>
                              <td>
                                  <?php echo $v['xsmlv'] ?>
                              </td>
                              <td>
                                  <?php echo $v['jlrhjl'] ?>
                              </td>
                              <td class="grid">
                                  <a href="<?php echo site_url('selected/setGridSystem/' . $v['symbol']); ?>" data-scode="<?php echo $v['scode'] ?>">
                                      <?php echo $v['gridInterval'] ?>
                                  </a>
                              </td>
                              <td class="suggest">b、s、h</td>
                          </tr>
                          <?php endforeach;?>
                      </tbody>
                  </table>
              </div>
          </div>
          <!-- end of .panel-body -->
      </div>
      <!-- end of .panel -->
      <script>
        // 设置菜单激活状态
      $('#selected').addClass('active');

      var xueqiuData;

      function setBackground(symbol, className) {
          $("tr#" + symbol).removeClass();
          $("tr#" + symbol).addClass(className);
      }

      function suggest(symbol, callback) {
          // 取得当前价格、净资产、pb范围、网格间距
          var tCurrent = Number($("tr#" + symbol + " td.price").text());
          var tNA = Number($("tr#" + symbol + " td.netAssets").text());
          var pb = $("tr#" + symbol + " td.pbRange").text();
          var pbs = new Array(); //定义一数组
          pbs = pb.split("-"); //字符分割
          var lowPB = Number(pbs[0]);
          var highPB = Number(pbs[1]);
          var gridInterval = Number($("tr#" + symbol + " td.grid").text());

          // 计算高低值
          var b8Price = tNA * lowPB;
          var b1Price = b8Price * (Math.pow(1 + gridInterval / 100, 7)).toFixed(2);
          var s8Price = tNA * highPB;
          var s1Price = s8Price * (Math.pow(1 - gridInterval / 100, 7)).toFixed(2);

          // 计算策略
          var suggestString = '';
          if (tCurrent <= b1Price) {
              suggestString = '买入';
          } else {
              if (tCurrent >= s8Price) {
                  suggestString = '清仓';
              } else {
                  if (tCurrent >= s1Price) {
                      suggestString = '卖出';
                  } else {
                      suggestString = '持有';
                  };
              };
          }
          callback(symbol, suggestString); /*使用回调函数*/
          return suggestString;
      }

      function getPriceFromXueqiu() {
          // 显示loading层
          var ii = layer.load();
          // 使用ajax方式获取数据，获取成功后，执行处理函数。
          // 在这里折腾了好久，明白了对跨域传递数据的限制，以及如何突破限制。
          $.getJSON("<?php echo site_url('curl/getStocksInfoFromXueqiu') ?>",
              function(data) {
                  // console.log(data);
                  for (var symbol in data) {
                      // 当前价格
                      var sPrice = "tr#" + symbol + " td.price";
                      var tCurrent = data[symbol].current;
                      $(sPrice).text(tCurrent);
                      // 当前PB
                      var sPB = "tr#" + symbol + " td.pb";
                      var tPB = Number(data[symbol].pb).toFixed(2);
                      $(sPB).text(tPB);
                      // 当前PE
                      var sPE = "tr#" + symbol + " td.pe";
                      // 显示小数点后两位
                      var tPE = Number(data[symbol].pe_lyr).toFixed(2);
                      $(sPE).text(tPE);
                      // 市值
                      var smarketCapital = "tr#" + symbol + " td.marketCapital";
                      // 显示小数点后两位
                      var tmarketCapital = (parseFloat(data[symbol].marketCapital) / 100000000).toFixed(0);
                      $(smarketCapital).text(tmarketCapital);
                      // 显示操作建议
                      var sSuggest = "tr#" + symbol + " td.suggest";
                      var tSuggest = suggest(symbol, setBackground);
                      $(sSuggest).text(tSuggest);
                  }
                  // 关闭loading层
                  layer.close(ii);
                  window.xueqiuData = data;
              });
      }
      // 弹出历史PB窗口
      function displayPBchar(scode) {
          layer.open({
              type: 2,
              title: '历史PB',
              shadeClose: true,
              shade: 0.8,
              closeBtn: 1,
              area: ['800px', '85%'],
              content: '<?php echo site_url("selected/displayHistoryPB") ?>' + '/' + scode //iframe的url
          });
      }
      // 弹出网格交易设置系统
      function displayGrid(scode) {
          layer.open({
              type: 2,
              title: '网格交易设定',
              shadeClose: true,
              shade: 0.8,
              closeBtn: 1,
              area: ['800px', '85%'],
              content: '<?php echo site_url("selected/setGridSystem") ?>' + '/' + scode //iframe的url
          });
      }
      $(document).ready(function() {
          getPriceFromXueqiu();
          // 股票信息定时刷新
          setInterval(getPriceFromXueqiu, 10000);

          // 设定历史pb链接点击功能
          $("td.historyPB a").each(function() {
              $(this).click(function() {
                  // 获取股票代码参数，以此为凭据获取新浪网
                  var scode = $(this).attr('data-scode');
                  // console.log(symbol);
                  displayPBchar(scode);
                  // 阻止原有事件的默认行为。
                  return false;
              });
          });

          // 设定网格交易系统链接点击功能
          $("td.grid a").each(function() {
              $(this).click(function() {
                  // 获取股票代码参数
                  var scode = $(this).attr('data-scode');
                  // console.log(symbol);
                  displayGrid(scode);
                  // 阻止原有事件的默认行为。
                  return false;
              });
          });

          // 为每个id添加删除功能
          $("td.id a").each(function() {
              $(this).click(function() {
                  // 获取股票代码参数
                  var code = $(this).parents("tr").attr('id');
                  // console.log($(this).parents("tr"));
                  $.post('<?php echo site_url("selected/updateSelected") ?>', {
                      scode: code,
                      flag: 0
                  }, function(data, textStatus, xhr) {
                      layer.msg('股票' + code + '已从自选股删除');
                      window.location.reload()
                  });
                  // console.log(symbol);
                  // displayGrid(scode);
                  // 阻止原有事件的默认行为。
                  return false;
              });
          });

          // 为行增加手动更新财务数据功能
          $("td.netAssets a").each(function() {
              $(this).click(function() {
                  // 获取股票代码参数
                  var scode = $.trim($(this).parents("tr").find("td.symbol").text());
                  // console.log(scode);
                  layer.confirm('确定要更新财务数据吗？', function(index) {
                      $.post('<?php echo site_url("Curl/getFinanceInfoByCode") ?>' + '/' + scode, function(data, textStatus, xhr) {
                          layer.msg('股票财务数据已更新！');
                          // window.location.reload()
                      });
                      // console.log(code);
                      // alert(code);
                      // layer.close(index);
                  });
              });
          });

      });
      </script>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

