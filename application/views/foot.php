  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      <p >Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="http://yangzh.cn">yangjh</a>.</strong>

  </footer>

</div>
<!-- ./wrapper -->
</body>
</html>
