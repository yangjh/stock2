!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>抓取公司历年财务数据入口列表<small>从同花顺网站抓取所有公司历年财务信息</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-info">
            <div class="box-body">
                <?php for ($i = 1; $i < count($step); $i++) {
	$url = 'curl/curlFinaceProgress/' . $step[$i - 1] . '/' . $step[$i];
	?>
                    <p><a href="<?php echo site_url($url) ?>" target="_black">抓取<?php echo $step[$i - 1] . '-' . $step[$i] ?>公司的财务数据</a></p>
                <?php }?>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
// 执行抓取任务
    $(function() {
        // 设置当前激活菜单样式
        $('#corp').addClass('active');
    });
</script>


