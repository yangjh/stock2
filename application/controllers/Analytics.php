<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analytics extends MY_Controller {

	// 显示分析工具
	public function index() {
		$this->loadview('analytics.html');
	}

	/**
	 * 分析指定代码的股票是否连续5个季度增长
	 */
	public function growth($symbol = '300104', $speed = 13) {
		// 读取最近5期的数据
		$data = $this->Symbol->getLastFiveData($symbol);
		// var_dump($data);exit;
		for ($i = 0; $i < 5; $i++) {
			if (($data[$i]['jlrzzl'] < $speed) | ($data[$i]['yyzsrzzl'] < $speed)) {
				$this->Stocks->setGrowthingFlag($symbol, false);
				return false;
			}
		}
		$this->Stock->setGrowthingFlag($symbol, true);

		return true;
	}

	/**
	 * 显示连续增长公司股票池
	 */
	public function displayGrowthing() {
		$this->loadview('growthing');
	}

	/**
	 * 显示个股详细信息
	 */
	public function displayStockInfoByCode($code = '') {
		$code = $this->uri->segment(3);
		$data['info'] = $this->Symbol->getStockInfoByCode($code);
		$data['stock'] = $this->Stocks->getByCode($code);
		$this->loadview('stockInfo', $data);
	}

	/**
	 * 计算公司连续增长的年份、近5期销售毛利率、近5期净利润含金量
	 */
	public function calculateByCode($symbol = '300104', $speed = 13) {
		echo '开始处理' . $symbol . '<br>';

		$data = $this->Symbol->getGrowthingData($symbol);

		// 如果表为空，则返回
		if (empty($data)) {
			echo $symbol . '表为空！<br>';
			return;
		}

		// 读取财务数据
		// var_dump($data);exit;
		for ($i = 0; $i < sizeof($data); $i++) {
			if (($data[$i]['jlrzzl'] < $speed) | ($data[$i]['yyzsrzzl'] < $speed)) {
				break;
			}
		}
		// $this->Stock->setSustainedGrowthingData( $symbol, $i );
		// 计算近5期销售毛利率、近5期净利润含金量
		$xsmlv = 0;
		$jlrhjl = 0;
		for ($j = 0; $j < 5; $j++) {
			$xsmlv = $xsmlv + $data[$j]['xsmlv'];
			$jlrhjl = $jlrhjl + $this->getJlrhjl($data[$j]['jyxjllje'], $data[$j]['jlr']);
		}
		$xsmlv = $xsmlv / 5;
		$jlrhjl = $jlrhjl / 5;
		$this->Stocks->updateExtraInfo($symbol, $i, $xsmlv, $jlrhjl);
		// echo $symbol . '分析完毕。' . '<br>';
	}

	public function getJlrhjl($a, $b) {
		if ($b != 0) {
			return $a / $b;
		} else {
			return null;
		}
	}

	/**
	 * 显示成长性分析进度视图
	 */
	public function displayGrowAnalProgress($value = '') {
		$this->loadview('growthingAnalyProgress');
	}

	/**
	 * 对全部股票进行分析，筛选双增速度不低于17的公司
	 */
	public function scanAllStock($speed = 17) {

		// 设置程序运行时间
		set_time_limit(0);

		// 重置进度内容
		$fileName = './application/tmp/growthing.txt';
		file_put_contents($fileName, ''); //写入缓存

		// 先选择所有公司代码
		$data = $this->Stocks->getTotalCode();
		// var_dump($data);exit();
		for ($i = 0; $i < sizeof($data); $i++) {
			$this->calculateByCode($data[$i]['scode'], $speed);

			// 将进度信息写入对应文件
			file_put_contents($fileName, round($i / sizeof($data) * 100, 0)); //写入缓存
			// var_dump($i);
		}

		// $this->displayGrowthing(3, 0, -2);
	}

	/**
	 * 分析毛利率最高的几家公司
	 */
	public function displayHighProfit($number = 200) {
		$data['growthing'] = $this->Stocks->getHighProfit($number);
		$data['info'] = array();
		$this->loadview('growthing', $data);
	}

	// 测试table插件
	public function table() {
		$this->load->view('test.html');
	}

	public function jsonData() {
		// 取消页面执行时间的限制
		set_time_limit(0);
		// 取得所有连续增长公司数据
		$data = $this->Stocks->totalGrowthing(3, 0, -2);
		// var_dump($data);exit;
		// 获取数组大小
		// 取得雪球cookie
		$cookies = $this->Curlmodel->getCookieByHttps('https://xueqiu.com/S/SZ300059');
		$step = 50;
		$k = intval(sizeof($data) / $step);
		$total = array();
		for ($i = 0; $i <= $k; $i++) {
			if (($i + 1) * $step > sizeof($data)) {
				$end = sizeof($data);
			} else {
				$end = ($i + 1) * $step;
			}
			$s = '';
			for ($j = $i * $step; $j < $end; $j++) {
				$s = $s . $data[$j]['symbol'] . ',';
			}
			// $cookies = $this->cookie_file;
			$url = 'https://xueqiu.com/v4/stock/quote.json?code=' . $s;
			$main[$i] = json2array(json_decode($this->Curlmodel->curl_https($url, $cookies)));
		}

		foreach ($main as $key => $value) {
			foreach ($main[$key] as $k => $v) {
				array_push($total, $main[$key][$k]);
			}
		}

		// 添加市值数据
		foreach ($data as $k => $v) {
			$data[$k]['marketCapital'] = $this->Curlmodel->getMC($data[$k]['symbol'], $total);
		}

		// 添加近三年净资产收益率

		// 添加近三年净资产内生增长率

		// 添加最近三个季度的净利润增长率

		echo json_encode($data);
	}

}
