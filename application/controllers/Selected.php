<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Selected extends MY_Controller {

	// 显示分析工具
	public function index() {
		$this->loadview('selected.html');
	}

	// 添加单个指定股票到自选股
	public function add2Selected($symbol = '') {
		$this->Stocks->updateSelectedFlag($symbol, 1);
		message('selected/displaySeletedStocks', '已添加到自选股');
	}

	// 显示自选股
	public function displaySeletedStocks() {
		// 选择数据
		$data['selectedStocks'] = $this->Stocks->getSelectedStocks();
		// 传递数据到视图
		$this->loadview('displaySelectedStocks', $data);
	}

	// 显示自选股历史pb及设置按钮
	public function displayHistoryPB($scode) {
		// 设置地址
		$data['url'] = 'http://biz.finance.sina.com.cn/company/compare/img_sjl_compare.php?stock_code=' . $scode . '&limit=2400';
		$data['scode'] = $scode;
		// 传递数据到视图
		$this->load->view('displayHistoryPB.html', $data);
	}

	// 接受自选股pb历史数据，并将其更新到数据库
	public function updateHistoryPB() {
		$scode = $this->input->post('scode');
		$lowpb = $this->input->post('lowpb');
		$toppb = $this->input->post('toppb');
		// $this->output->enable_profiler(TRUE);
		$this->Stocks->updateSelectedpb($scode, $lowpb, $toppb);
	}

	// 设定指定股票的网格交易系统
	public function setGridSystem($scode) {
		// 获得该股票的财务信息
		// $this->output->enable_profiler(TRUE);
		// var_dump($scode);
		$data['stockInfo'] = $this->Stocks->getGridinfo($scode);
		$data['netAssets'] = $this->Symbol->getStockNetAssets($scode);
		// var_dump($data);exit;
		$this->load->view('setGridSystem.html', $data);
	}

	// 接受网格数据，并将其更新到数据库
	public function updateGridInterval() {
		$symbol = $this->input->post('symbol');
		$gridInterval = $this->input->post('gridInterval');
		// $this->output->enable_profiler(TRUE);
		// var_dump($symbol,$gridInterval);
		$this->Stocks->updateGridInterval($symbol, $gridInterval);
	}

	// 接受代码，并将其更新到数据库
	public function updateSelected() {
		$scode = substr($this->input->post('scode'), -6);
		$flag = $this->input->post('flag');
		// $this->output->enable_profiler(TRUE);
		$this->Stocks->updateSelected($scode, $flag);
	}

	public function updateMultiSelected() {
		$symbols = $this->input->post('symbols');
		$data = explode(',', $symbols);
		// var_dump($symbols,$data);
		foreach ($data as $key => $value) {
			$scode = substr($value, -6);
			$this->Stocks->updateSelected($scode, 1);
		}
		// $this->output->enable_profiler(TRUE);
		// $this->Stocks->updateSelected($scode, $flag);
	}
}
